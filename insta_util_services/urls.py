"""insta_util_services URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from polls.views import (
    account_top, add_hashtag, autolike,
    LoginCreateView, service_login, register_autolike,
    logout, logs
)

urlpatterns = [
    path('login', LoginCreateView.as_view(), name="login"),
    path('add_hashtag', add_hashtag, name='add_hashtag'),
    path('service_login', service_login, name='service_login'),
    path('logout', logout, name='logout'),
    path('logs', logs, name="logs"),
    path('autolike', autolike, name='autolike'),
    path('register/autolike', register_autolike, name='register_autolike'),
    path('<str:username>', account_top, name='account_top'),
]
