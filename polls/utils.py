# pylint: disable=E0401
import base64
import os
import random
import time
from hashlib import sha256
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from logging import getLogger, StreamHandler, FileHandler, Formatter, DEBUG
from Crypto.Cipher import AES
from .models import Action

USER_PATH = '/var/log/django/insta_util_services/{}/'

USER_LOG_PATH = USER_PATH + 'user_log/'
ACCESS_LOG_PATH = 'access'
ACTION_LOG_PATH = 'action'
SYSTEM_LOG_PATH = 'system'

USER_FILE_PATH = USER_PATH + 'files/'

loggers = {}


def sleep(count=3):
    """
    処理の一時停止を行う.
    パラメータに指定された秒数停止する(既定値3)
    """
    time.sleep(count)


def save(driver, fn='test.png'):
    """ スクリーンショット """
    sleep()
    driver.save_screenshot(fn)


def get_logged_driver(username, password, logger):
    """ インスタグラムにログインした状態のChrome Driver(Browser)を返却 """
    options = Options()

    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-gpu')
    options.add_argument('--window-size=1280,1024')
    options.add_argument('--disable-setuid-sandbox')

    login_url = 'https://www.instagram.com/accounts/login/?source=auth_switcher'

    username_selector = '#react-root [name=username]'
    password_selector = '#react-root [name=password]'

    login_account_selector = f"nav a[href*='{username}']"
    login_page_btn_selector = "//a[contains(text(), 'Log in')]"
    top_url = 'https://www.instagram.com/'

    try:
        driver = webdriver.Chrome(chrome_options=options)
        driver.get(login_url)

        sleep()

        try:
            transition_button = driver.find_element_by_xpath(
                login_page_btn_selector)
            transition_button.click()
            sleep()
        except NoSuchElementException:
            pass

        username_field = driver.find_element_by_css_selector(username_selector)
        username_field.send_keys(username)

        password_field = driver.find_element_by_css_selector(password_selector)
        password_field.send_keys(password)

        password_field.send_keys(Keys.RETURN)

        sleep()

        try:
            driver.get(top_url)
            sleep()
            driver.find_element_by_css_selector(login_account_selector)
        except NoSuchElementException as e:
            logger.exception('Could not log in: %e', e)
            raise

    except Exception as e:
        driver.quit()
        raise e

    return driver


class AESCipher(object):
    def __init__(self, key, block_size=32):
        self.bs = block_size
        if len(key) >= len(str(block_size)):
            self.key = key[:block_size]
        else:
            self.key = self._pad(key)

    def generate_salt(self, digit_num):
        digits_and_alphabets = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return "".join(random.sample(digits_and_alphabets, digit_num)).encode()

    def encrypt(self, raw):
        raw = self._pad(raw)
        salt = 'G8f6YgRlOaLrTQpb'.encode()
        salted = ''.encode()
        dx = ''.encode()
        while len(salted) < 48:
            hash = dx + self.key.encode() + salt
            dx = sha256(hash).digest()
            salted = salted + dx

        key = salted[0:32]
        iv = salted[32:48]

        cipher = AES.new(key, AES.MODE_CBC, iv)
        return base64.b64encode(salt + cipher.encrypt(raw)).decode()

    def decrypt(self, enc):
        enc = base64.b64decode(enc.encode())
        salt = enc[0:16]
        rounds = 3

        data00 = self.key.encode() + salt
        hash = {}
        hash[0] = sha256(data00).digest()
        result = hash[0]
        for i in range(1, rounds):
            hash[i] = sha256(hash[i - 1] + data00).digest()
            result += hash[i]

        key = result[0:32]
        iv = result[32:48]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return cipher.decrypt(enc[16:]).decode()

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]


def get_user_logger(user_id, path):
    global loggers

    key = str(user_id) + path

    if loggers.get(key):
        return loggers.get(key)
    else:
        logpath = USER_LOG_PATH.format(user_id)

        if not os.path.isdir(logpath):
            os.makedirs(logpath)

        logpath = logpath + path + '.log'

        if not os.path.isfile(logpath):
            with open(logpath, 'w') as f:
                pass

        logger = getLogger(key)
        logger.setLevel(DEBUG)

        handler_format = Formatter(' '.join([
            "[%(levelname)s]",
            "%(asctime)s: ",
            "%(message)s",
        ]))

        stream_handler = StreamHandler()
        stream_handler.setLevel(DEBUG)

        stream_handler.setFormatter(handler_format)

        file_handler = FileHandler(logpath, 'a')
        file_handler.setLevel(DEBUG)
        file_handler.setFormatter(handler_format)

        logger.addHandler(stream_handler)
        logger.addHandler(file_handler)

        loggers[key] = logger

        return logger

def close_logger():
    global loggers

    for key in loggers:
        logger = loggers.get(key)
        for handler in logger.handlers:
            logger.removeHandler(handler)


def save_binary(driver, url, filepath):
    js = """
    var getBinaryResourceText = function(url) {
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.overrideMimeType('text/plain; charset=x-user-defined');
        req.send(null);
        if (req.status != 200) return '';

        var filestream = req.responseText;
        var bytes = [];
        for (var i = 0; i < filestream.length; i++){
            bytes[i] = filestream.charCodeAt(i) & 0xff;
        }

        return bytes;
    }
    """
    js += "return getBinaryResourceText(\"{url}\");".format(url=url)

    data_bytes = driver.execute_script(js)
    with open(filepath, 'wb') as bin_out:
        bin_out.write(bytes(data_bytes))


def like_btn_push(driver, logger, keyword, account):
    like_btn_selector = "[role=dialog] button [aria-label=いいね！]"
    username_selector = '[role=dialog] header h2 a'
    image_src_selector = '[role=dialog] img'

    images = driver.find_elements_by_css_selector(image_src_selector)

    # ユーザ情報
    username = driver.find_element_by_css_selector(username_selector).text
    url = driver.find_element_by_css_selector(username_selector).get_attribute('href')
    user_img_url = images[0].get_attribute('src')

    # 画像情報
    img_url = images[1].get_attribute('src')
    current_url = driver.current_url

    try:
        like_btn = driver.find_element_by_css_selector(
            like_btn_selector)
        like_btn.click()
        logger.info(account.username + ':' + username + 'さんの投稿をいいねしました: #' + keyword)
        Action(
            account_id = account.id,
            user = username,
            url = url,
            user_img_url = user_img_url,
            action_type = 'liked',
            img_url = img_url,
            post_url = current_url,
        ).save()
    except NoSuchElementException:
        pass
    except Exception as e:
        print(e)