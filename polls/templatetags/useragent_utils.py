# pylint: disable=E0401
from django import template
from django_user_agents.utils import get_user_agent

register = template.Library()


@register.filter
def is_pc(request):
    return get_user_agent(request).is_pc
