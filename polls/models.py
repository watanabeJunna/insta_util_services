# pylint: disable=E0401
from django.db import models
from django.utils import timezone


class Account(models.Model):
    class Meta:
        unique_together = (('username', 'password'),)

    username = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    autolike = models.BooleanField(default=False)


class Hashtag(models.Model):
    class Meta:
        unique_together = (('account_id', 'tagname'),)

    account_id = models.IntegerField()
    tagname = models.CharField(max_length=128)

class Action(models.Model):
    class Meta:
        pass

    user = models.CharField(max_length=256)
    url =  models.CharField(max_length=2048)
    user_img_url = models.CharField(max_length=2048)
    action_type = models.CharField(max_length=256)
    account_id = models.IntegerField()
    img_url = models.CharField(max_length=2048, null=True)
    post_url = models.CharField(max_length=2048, null=True)
    datetime = models.DateTimeField(default=timezone.now)
