# pylint: disable=E0401
import json
import os
import urllib.parse
import traceback
from django_user_agents.utils import get_user_agent
from django.http import HttpResponse, HttpResponseRedirect
from django.template.context_processors import csrf
from django.urls import reverse
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect, render_to_response
from django.views.generic import CreateView
from django.db.utils import IntegrityError
from selenium.common.exceptions import NoSuchElementException
from .models import Account, Hashtag, Action
from .forms import AccountForm
from .utils import *

""" 暗号化、複合化を行うインスタンス """
cipher = AESCipher(os.environ['DJANGO_HASH_SALT'])

TEMPLATE_EXTENSION = '.html.j2'


class LoginCreateView(CreateView):
    model = Account
    form_class = AccountForm
    template_name = f"login{TEMPLATE_EXTENSION}"

    def get(self, request, *args, **kwargs):
        self.object = None
        if 'logged' in request.session:
            return redirect(f"/{request.session['username']}")
        return super().get(request, *args, **kwargs)

    def get_success_url(self):
        return f"/{self.request.POST['username']}"

    def form_valid(self, form):
        self.request.session['logged'] = True
        self.request.session['username'] = self.request.POST['username']
        self.request.session['password'] = cipher.encrypt(
            self.request.POST['password'])
        return HttpResponseRedirect(self.get_success_url())


def account_top(request, username):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    if username != request.session['username']:
        return HttpResponse(status=404)

    username = request.session['username']
    password = request.session['password']

    user = Account.objects.filter(username=username, password=password)

    if bool(user):
        request.session['account_id'] = user.get().id
    else:
        Account(username=username, password=password).save()
        request.session['account_id'] = Account.objects.filter(
            username=username, password=password).get().id

    context = {}
    context['hashtags'] = Hashtag.objects.filter(
        account_id=request.session['account_id']).all()
    context['autolike'] = str(user.get().autolike).lower()

    return render(request, f"main{TEMPLATE_EXTENSION}", context)


def add_hashtag(request):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    hashtag = request.POST['hashtag']

    account_id = request.session['account_id']
    redirect_url = f"/{request.session['username']}"

    if hashtag == "" :
        return redirect(redirect_url)

    if hashtag[0] == "#":
        hashtag = hashtag[0:]

    """ unique error hashtag.tagname """
    try:
        Hashtag(tagname=hashtag, account_id=account_id).save()
    except IntegrityError as e:
        pass

    return redirect(redirect_url)


def service_login(request):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    if request.method != 'POST':
        return redirect(reverse('login'))

    # JavaScript側で制御すること
    if 'loggin_success' in request.session:
        return HttpResponse(status=200)

    driver = None
    username = request.session['username']
    password = cipher.decrypt(request.session['password'])

    access_logger = get_user_logger(
        request.session['account_id'], ACCESS_LOG_PATH
    )
    system_logger = get_user_logger(
        request.session['account_id'], SYSTEM_LOG_PATH
    )

    try:
        driver = get_logged_driver(username, password, system_logger)
    except Exception as e:
        system_logger.exception(e)
        access_logger.info(f"{request.session['username']} ログインに失敗しました")
        return HttpResponse(status=500)
    finally:
        if driver != None:
            driver.quit()

    # JavaScript側で制御すること
    request.session['loggin_success'] = True

    access_logger.info(f"{request.session['username']} ログイン")

    close_logger()

    return HttpResponse(status=200)


def logout(request):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    get_user_logger(
        request.session['account_id'], ACCESS_LOG_PATH
    ).info(f"{request.session['username']} ログアウト")

    request.session.clear()
    close_logger()

    return redirect(reverse('login'))


def logs(request):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    context = {}
    logpath = USER_LOG_PATH.format(request.session['account_id'])
    
    access_log_path = logpath + ACCESS_LOG_PATH + '.log'
    action_log_path = logpath + ACTION_LOG_PATH + '.log'

    if not os.path.isfile(access_log_path):
        with open(access_log_path, 'w') as f:
            pass

    if not os.path.isfile(action_log_path):
        with open(action_log_path, 'w') as f:
            pass
    
    with open(access_log_path, 'r') as f:
        context['access_log'] = f.read()

    with open(action_log_path, 'r') as f:
        context['action_log'] = f.read()

    context['access_log'] = reversed(context['access_log'].split('\n'))
    context['action_log'] = reversed(context['action_log'].split('\n'))
    context['action'] = Action.objects.all()
    print(context['action'])

    return render(request, f"logs{TEMPLATE_EXTENSION}", context)

def register_autolike(request):
    if 'logged' not in request.session:
        return redirect(reverse('login'))

    if request.method != 'POST':
        return redirect(reverse('login'))

    content = json.loads(request.body.decode('utf-8'))['data']

    system_logger = get_user_logger(
        request.session['account_id'], SYSTEM_LOG_PATH
    )

    try:
        user = Account.objects.get(id=request.session['account_id'])
        user.autolike = content
        user.save()
    except Exception as e:
        system_logger.exception(e)
        raise
    finally:
        close_logger()
    
    return HttpResponse(status=200)

def autolike(request):
    driver = None

    for account in Account.objects.all():
        if account.autolike:
            LIMIT = 10
            INTERVAL = 30

            account_id = account.id

            driver = None
            action_logger = get_user_logger(
                account_id, ACTION_LOG_PATH
            )
            system_logger = get_user_logger(
                account_id, SYSTEM_LOG_PATH
            )

            username = account.username
            password = cipher.decrypt(account.password)
            keywords = [kw.tagname for kw in Hashtag.objects.filter(
                account_id=account.id).all()]

            tag_search_url = "https://www.instagram.com/explore/tags/{}/?hl=ja"

            posts_selector = 'a div img'
            next_btn_selector = ".coreSpriteRightPaginationArrow"

            keyword = ''

            try:
                driver = get_logged_driver(username, password, system_logger)

                for keyword in keywords:
                    action_logger.info('いいね処理を開始します。 #' + keyword)
                    driver.get(tag_search_url.format(
                        urllib.parse.quote(keyword)
                    ))

                    sleep()
                    driver.implicitly_wait(10)

                    # 投稿のクラス名を取得
                    target_class = driver.find_element_by_css_selector(
                        posts_selector).get_attribute('class')

                    post_selector = f"//img[@class='{target_class}']/ancestor-or-self::a"

                    # ポップアップを表示
                    driver.find_element_by_xpath(post_selector).click()
                    sleep()

                    like_btn_push(driver, action_logger, keyword, account)

                    for c in range(LIMIT):
                        # 次の投稿に移動
                        driver.find_element_by_css_selector(next_btn_selector).click()
                        sleep()

                        like_btn_push(driver, action_logger, keyword, account)

                        # ?分間毎に?回いいねをすると制限を受ける
                        if c != 0 and c % INTERVAL == 0:
                            sleep(120)

                    action_logger.info(str(LIMIT) + '回いいねしました。: #' + keyword)

            except Exception as e:
                system_logger.exception(e)
                action_logger.info('いいね処理に失敗しました: ' + keyword)
                raise
            finally:
                if driver != None:
                    driver.quit()
                close_logger()
    
    return HttpResponse(status=200)


