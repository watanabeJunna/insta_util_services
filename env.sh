$ cat /etc/system-release
Amazon Linux release 2 (Karoo)

$ uname -r
4.14.77-81.59.amzn2.x86_64

$ sudo sh -c 'echo "ClientAliveInterval 60" >> /etc/ssh/sshd_config'
$ sudo sh -c 'echo "ClientAliveCountMax 5" >> /etc/ssh/sshd_config'

$ sudo ln -sf  /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
$ sudo sh -c 'echo ZONE="Asia/Tokyo" > /etc/sysconfig/clock'
$ sudo sh -c 'echo UTC=false > /etc/sysconfig/clock'

$ date

$ sudo yum install git gcc zlib-devel bzip2 bzip2-devel readline readline-devel sqlite sqlite-devel openssl openssl-devel -y
$ git clone https://github.com/yyuu/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
$ echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
$ source ~/.bash_profile

$ pyenv -v
pyenv 1.2.8-5-gec9fb549

$ pyenv install 3.6.2
Downloading Python-3.6.2.tar.xz...
-> https://www.python.org/ftp/python/3.6.2/Python-3.6.2.tar.xz
Installing Python-3.6.2...
Installed Python-3.6.2 to /home/ec2-user/.pyenv/versions/3.6.2

$ pyenv global 3.6.2
$ pyenv rehash

$ python --version
Python 3.6.2

$ curl https://intoli.com/install-google-chrome.sh | bash

$ sudo sh -c 'echo "[CentOS-base]
name=CentOS-6 - Base
mirrorlist=http://mirrorlist.centos.org/?release=6&arch=x86_64&repo=os
gpgcheck=1
gpgkey=http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6

#released updates
[CentOS-updates]
name=CentOS-6 - Updates
mirrorlist=http://mirrorlist.centos.org/?release=6&arch=x86_64&repo=updates
gpgcheck=1
gpgkey=http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6

#additional packages that may be useful
[CentOS-extras]
name=CentOS-6 - Extras
mirrorlist=http://mirrorlist.centos.org/?release=6&arch=x86_64&repo=extras
gpgcheck=1
gpgkey=http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6" \
> /etc/yum.repos.d/centos.repo'

$ sudo rpm --import http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-6 
$ sudo yum -y install GConf2
$ curl -O https://chromedriver.storage.googleapis.com/2.37/chromedriver_linux64.zip
$ unzip chromedriver_linux64.zip
$ sudo mv chromedriver /usr/local/bin/

$ mkdir ~/noto && cd ~/noto
$ wget https://noto-website-2.storage.googleapis.com/pkgs/Noto-hinted.zip
$ unzip Noto-hinted.zip
$ sudo mkdir -p /usr/share/fonts/opentype/noto
$ sudo cp *otf *ttf /usr/share/fonts/opentype/noto
$ sudo fc-cache -f -v # Enter押下

$ pip install --upgrade pip
Collecting pip
  Downloading https://files.pythonhosted.org/packages/c2/d7/90f34cb0d83a6c5631cf71dfe64cc1054598c843a92b400e55675cc2ac37/pip-18.1-py2.py3-none-any.whl (1.3MB)

$ pip --version
pip 18.1 from /home/ec2-user/.pyenv/versions/3.6.2/lib/python3.6/site-packages/pip (python 3.6) 

$ echo "Django==2.1.4
django-crispy-forms==1.7.2
django-user-agents==0.3.2
django-widget-tweaks==1.4.3
gunicorn==19.9.0
Pillow==5.3.0
pycrypto==2.6.1
python-memcached==1.59
pytz==2018.7
PyYAML==3.13
qrcode==6.0
selenium==3.141.0
six==1.12.0
ua-parser==0.8.0
urllib3==1.24.1
user-agents==1.1.0" | while read e; do
    pip install $e 
done

$ rm -rf noto
$ rm -f chromedriver_linux64.zip

$ echo "Host github github.com
  HostName github.com
  Port 22
  User git
  IdentityFile ~/.ssh/id_rsa
"  > ~/.ssh/config

$ echo "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA2eZYb72qVJkVfvfg6No+gxF5YkJnw1rQcEbUuMWsQouCfRHL
uk05U5G43+iNBETO1ChYKvVgOAnAe8nBfPjMl3Y4q+z8aCJxiOgr1/Xl6aEsl6OF
Bq0tTtLbiyQrWm0xseSHQe7VOb5vnh0HrGlv1E0u5Q8ygMIs55AVQMxe2Pq2OhRy
9L00edPLj76+7xBHUeReZQcRe+eA3T/I8Z/tCHdWqCBlsDidLizUOaWbLVp0IuDd
duTQHWs0Xkn+1INsegEps+gu/hkGkg2K8SBjaEuCTk82I7/4rsYqOGzW4KdPI2iI
uYxLoxSWxkP0r94UXU503gTmPHUfD6jJxDsoaQIDAQABAoIBAQDJ4J1iuUgLpODD
h4g6c6ii6heZr4hO995Rh24h2hIkfPhiQ5CKhG1uPX4ZBG1R3SX6bX5yHnAMuww4
I849qIx0bzVGJAQdvnuNqfyk0s81yvtzai4e52cotjZtDQ+VccEu3Vlq/olSPFtR
E/g+BllzSib+VTwnCr9rspPeOvpM7xQiQRl7jsdcmo1+fpDkvsYHYWgBhhVY5XZP
0YOTHV/J+HKP5k5/+JPskT7eoEP01XimuG9Pnt9vkJ+u3cwEHlZuNd0tetXhSbX5
5Gu9GqBu40H7zU+LbwtT99DTiNhle9x3yZ1I4U83xe+Y+U2cXU12HDnRUZro3UK1
69JXlFnhAoGBAP0U4+ZTFt2LyIPIFD63tSsGoG4CBCmqWvn9kF1BxSGh151Lit8K
TjJ26LLRT8uSeZ8o3MdRrA9VIlDb/YBR4AGV5rAObaf819sWlY/BXgQsPnt5OnCn
DbDv/uD5FvudKlqUdrtW1WccO/L9eRhzoY4VeyLiBZjKrWYQrZKZ7O6dAoGBANxp
mMIkDl3UhyTU4R7IT4m4j3iIZFSCJVNjbya5Uf3SNCy3PMgMgAIuxiK06eg9ami8
uXntEcP3zRqXe7ZIYJJGzcBJn8jWGWFd/uSj5+xt3M5wtFubPp4uh1B8M7bCHeNq
r0y2kF95sfrR68La59Ao1m6fb92yK2D7WD2rT3E9AoGAC6aPi0Xd8psBwxdN30lD
vNRURFwxZt2hzIpgv/pMIe06aNJWdUgbK3WP5tN98egvUyVlbIOVASuxJbnK5MJg
JpKJCSfQ4n/gTASxI+oJ8vU6dyKwjwkRt0iFGkQ9S54DmiHliWBI+LMzC0+h6H9C
O7E/du6ixVLnQqCX7PzLDH0CgYEAjhnWTnQIfx3h8zD74miqPtQqSz9Dz+gIQHa+
1mxsHARFdv67xQ2qCb42J3ITy5Ks49Gokp7w8TL1nMtFCwQmnqffRrUwur6tpkR/
bNDvWVKLJN0sqlKwUDMXMiChBpanxlKlqgOZmLRQtrBYG0weX+m0NaW87WVM6j2h
akDj+tECgYEAnlgMTT+tquYnzHuEOOyWTIoVNLWvf90vMynCQPX4xL3o6ltJ15Pe
y5SyIqrNO0xpAg27oJ1nY7MJlTmjxpuYX58/9WdQCdHK4dlqU1s9G4dLhCiuN4V0
Tonmmb/gZGSK3nQy5tY0dVE1PAId5Hj9PKjFTQ8LI1OfWu6apyy0WNg=
-----END RSA PRIVATE KEY-----
" > ~/.ssh/id_rsa

$ echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ5lhvvapUmRV+9+Do2j6DEXliQmfDWtBwRtS4xaxCi4J9Ecu6TTlTkbjf6I0ERM7UKFgq9WA4CcB7ycF8+MyXdjir7PxoInGI6CvX9eXpoSyXo4UGrS1O0tuLJCtabTGx5IdB7tU5vm+eHQesaW/UTS7lDzKAwiznkBVAzF7Y+rY6FHL0vTR508uPvr7vEEdR5F5lBxF754DdP8jxn+0Id1aoIGWwOJ0uLNQ5pZstWnQi4N125NAdazReSf7Ug2x6ASmz6C7+GQaSDYrxIGNoS4JOTzYjv/iuxio4bNbgp08jaIi5jEujFJbGQ/Sv3hRdTnTeBOY8dR8PqMnEOyhp ec2-user@ip-172-31-46-52.us-east-2.compute.internal
" > ~/.ssh/id_rsa.pub

$ chmod 600 ~/.ssh/config ~/.ssh/id_rsa ~/.ssh/id_rsa.pub

$ ssh -T git@gitlab.com

$ cd
$ git init
$ git remote add origin git@gitlab.com:watanabeJunna/insta_util_services.git
$ git clone git@gitlab.com:watanabeJunna/insta_util_services.git
$ cd ~/insta_util_services/
$ ./manage.py makemigrations
$ ./manage.py migrate
$ ~/.pyenv/shims/python3 ./manage.py runserver 0.0.0.0:8000

$ sudo sh -c 'echo "[Unit]
Description=gunicorn daemon
After=network.target

[Service]
NoNewPrivileges=yes
User=ec2-user
Group=ec2-user
WorkingDirectory=/home/ec2-user/insta_util_services/
ExecStart=/home/ec2-user/.pyenv/shims/python3 ./manage.py runserver 0.0.0.0:8000

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/insta_util_services.service'

$ sudo systemctl daemon-reload
$ sudo systemctl start insta_util_services.service
$ sudo systemctl status insta_util_services

$ echo '#!/usr/bin/env bash

echo "$(curl -s ifconfig.me)"
' > ./myip && sudo mv ./myip /usr/local/bin/myip

$ chmod 755 /usr/local/bin/myip
$ myip

$ echo '# insta util services: kick auto like
* 6 * * * curl "locahost:8000/autolike" > /dev/null 2>&1
' > ec2-user && sudo mv ./ec2-user /var/spool/cron/.

$ sudo chmod 600 /var/spool/cron/ec2-user
$ crontab -l

# ???
$ sudo mkdir /var/log/django
$ sudo chown ec2-user:ec2-user /var/log/django